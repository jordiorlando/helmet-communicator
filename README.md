# Helmet Communicator

A does-it-all motorbike helmet communicator.

## Features

- Microchip BM83 bluetooth audio module
- Rider-to-rider communications on the unlicensed 915MHz band (or 868MHz in Europe)
- FM and weather-band radio receiver (optional?)
- Arbitrary audio source mixing (listen to music while talking to your friends)
- Long battery life and USB-C fast charging
- Optional wireless charging
- Optional handlebar-mounted controls
- On-device voice control? (using an FPGA)