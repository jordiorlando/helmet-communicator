PCBNEW-LibModule-V1  
# encoding utf-8
Units mm
$INDEX
GCT_USB4110-GF-A
$EndINDEX
$MODULE GCT_USB4110-GF-A
Po 0 0 0 15 00000000 00000000 ~~
Li GCT_USB4110-GF-A
Cd 
Sc 00000000
At SMD
Op 0 0 0
.SolderMask 0
.SolderPaste 0
T0 -2.06 -3.135 1 1 0 0.05 N V 21 "GCT_USB4110-GF-A"
T1 -1.425 7.265 1 1 0 0.05 N V 21 "VAL**"
DS -4.47 -1.07 -4.47 6.28 0.1 27
DS -4.47 6.28 4.47 6.28 0.1 27
DS 4.47 6.28 4.47 -1.07 0.1 27
DS 4.47 -1.07 -4.47 -1.07 0.1 27
DS -6.45 -1.9 6.45 -1.9 0.05 26
DS 6.45 -1.9 6.45 6.53 0.05 26
DS 6.45 6.53 -6.45 6.53 0.05 26
DS -6.45 6.53 -6.45 -1.9 0.05 26
DS -4.47 4.79 -4.47 6.28 0.2 21
DS -4.47 6.28 4.47 6.28 0.2 21
DS 4.47 6.28 4.47 4.79 0.2 21
DS -4.47 0.86 -4.47 2.07 0.2 21
DS 4.47 0.86 4.47 2.07 0.2 21
DC -3.78 -2.23 -3.68 -2.23 0.2 21
DC -3.78 -2.23 -3.68 -2.23 0.2 27
$PAD
Sh "A1/B12" R 0.6 1.15 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.2 -1.075
$EndPAD
$PAD
Sh "A4/B9" R 0.6 1.15 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.4 -1.075
$EndPAD
$PAD
Sh "B4/A9" R 0.6 1.15 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.4 -1.075
$EndPAD
$PAD
Sh "B1/A12" R 0.6 1.15 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.2 -1.075
$EndPAD
$PAD
Sh "B8" R 0.3 1.15 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.75 -1.075
$EndPAD
$PAD
Sh "B5" R 0.3 1.15 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.75 -1.075
$EndPAD
$PAD
Sh "A5" R 0.3 1.15 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.25 -1.075
$EndPAD
$PAD
Sh "A8" R 0.3 1.15 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.25 -1.075
$EndPAD
$PAD
Sh "B7" R 0.3 1.15 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -0.75 -1.075
$EndPAD
$PAD
Sh "B6" R 0.3 1.15 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 0.75 -1.075
$EndPAD
$PAD
Sh "A6" R 0.3 1.15 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -0.25 -1.075
$EndPAD
$PAD
Sh "A7" R 0.3 1.15 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 0.25 -1.075
$EndPAD
$PAD
Sh "S1" R 2.18 2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.11 -0.5
$EndPAD
$PAD
Sh "S2" R 2.18 2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.11 -0.5
$EndPAD
$PAD
Sh "S3" R 2.18 2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.11 3.43
$EndPAD
$PAD
Sh "S4" R 2.18 2 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.11 3.43
$EndPAD
$PAD
Sh "Hole" C 0.65 0.65 0 0 0
Dr 0.65 0 0
At HOLE N 00E0FFFF
Po -2.89 0
$EndPAD
$PAD
Sh "Hole" C 0.65 0.65 0 0 0
Dr 0.65 0 0
At HOLE N 00E0FFFF
Po 2.89 0
$EndPAD
$EndMODULE GCT_USB4110-GF-A
